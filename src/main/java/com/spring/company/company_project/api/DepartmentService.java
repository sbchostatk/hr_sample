package com.spring.company.company_project.api;

import com.spring.company.company_project.dto.DepartmentDTO;
import com.spring.company.company_project.dto.EmployeeDTO;

import java.text.ParseException;
import java.util.List;

public interface DepartmentService {
    DepartmentDTO getDepartmentById(Integer id);

    DepartmentDTO insertDepartment(DepartmentDTO departmentDTO);

    List<DepartmentDTO> getAll();

    DepartmentDTO update(DepartmentDTO departmentDTO);

    List<DepartmentDTO> getAllByDepartmentName(String department_name);

    List<DepartmentDTO> getAllByLastName(String last_name);
}
