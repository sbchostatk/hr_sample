package com.spring.company.company_project.api;

import com.spring.company.company_project.dto.EmployeeDTO;
import com.spring.company.company_project.dto.LocationDTO;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface EmployeeService {
    EmployeeDTO getEmployeeById(Integer id);

    EmployeeDTO insertEmployee(EmployeeDTO employeeDTO);

    List<EmployeeDTO> getAll();

    EmployeeDTO update(EmployeeDTO employeeDTO);

    List<EmployeeDTO> getAllByLastName(String last_name);

    List<EmployeeDTO> getAllByFirstName(String first_name);

    List<EmployeeDTO> getAllByEmail(String email);

    List<EmployeeDTO> getAllByDate(String start, String end) throws ParseException;
}
