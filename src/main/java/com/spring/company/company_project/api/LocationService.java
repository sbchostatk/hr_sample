package com.spring.company.company_project.api;

import com.spring.company.company_project.dto.LocationDTO;

import java.util.List;

public interface LocationService {
    LocationDTO getLocationById(Integer id);

    LocationDTO insertLocation(LocationDTO locationDTO);

    List<LocationDTO> getAll();
}
