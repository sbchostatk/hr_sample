package com.spring.company.company_project.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationDTO {
    @Schema(title = "Идентификатор")
    private int location_id;

    @Schema(title = "Адрес")
    private String street_address;

    @Schema(title = "Город")
    private String city;
}
