package com.spring.company.company_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentDTO {
    private int department_id;
    private String department_name;
    private int manager_id;
    private int location_id;
}
