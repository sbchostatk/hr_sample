package com.spring.company.company_project.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentEntity {
    private int department_id;
    private String department_name;
    private int manager_id;
    private int location_id;

    @JsonIgnoreProperties("locations")
    private List<LocationEntity> locations = new ArrayList<>();
}
