package com.spring.company.company_project.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationEntity {
    private int location_id;

    private String street_address;

    private String city;
}
