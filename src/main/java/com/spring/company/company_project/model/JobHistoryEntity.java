package com.spring.company.company_project.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobHistoryEntity {
    private int employee_id;
    private Date start_date;
    private Date end_date;
    private String job_id;
    private int department_id;
}
