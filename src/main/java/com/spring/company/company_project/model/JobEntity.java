package com.spring.company.company_project.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobEntity {
    private String job_id;
    private String job_title;
    private int min_salary;
    private int max_salary;
}
