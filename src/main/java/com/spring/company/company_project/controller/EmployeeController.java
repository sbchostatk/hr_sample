package com.spring.company.company_project.controller;

import com.spring.company.company_project.api.EmployeeService;
import com.spring.company.company_project.api.LocationService;
import com.spring.company.company_project.dto.EmployeeDTO;
import com.spring.company.company_project.dto.LocationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/{employee_id}")
    public EmployeeDTO getEmployeeById(@PathVariable Integer employee_id) {
        return employeeService.getEmployeeById(employee_id);
    }

    @PostMapping("/insert")
    public EmployeeDTO insertEmployee(@RequestBody EmployeeDTO employeeDTO) {
        return employeeService.insertEmployee(employeeDTO);
    }

    @PostMapping("/update")
    public EmployeeDTO updateEmployee(@RequestBody EmployeeDTO employeeDTO) {
        return employeeService.update(employeeDTO);
    }

    @GetMapping("/getAll")
    public List<EmployeeDTO> getAllEmployees() {
        return employeeService.getAll();
    }

    @GetMapping("/getAllByFirstName/{first_name}")
    public List<EmployeeDTO> getAllByFirstName(@PathVariable String first_name) {
        return employeeService.getAllByFirstName(first_name);
    }

    @GetMapping("/getAllByLastName/{last_name}")
    public List<EmployeeDTO> getAllByLastName(@PathVariable String last_name) {
        return employeeService.getAllByLastName(last_name);
    }

    @GetMapping("/getAllByEmail/{email}")
    public List<EmployeeDTO> getAllByEmail(@PathVariable String email) {
        return employeeService.getAllByEmail(email);
    }

    @GetMapping("/getAllByDate/{start_date}/{end_date}")
    public List<EmployeeDTO> getAllByEmail(@PathVariable String start_date, @PathVariable String end_date) throws ParseException {
        return employeeService.getAllByDate(start_date, end_date);
    }
}
