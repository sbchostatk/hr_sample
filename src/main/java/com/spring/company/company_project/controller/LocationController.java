package com.spring.company.company_project.controller;

import com.spring.company.company_project.api.LocationService;
import com.spring.company.company_project.dto.LocationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/locations")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @GetMapping("/{location_id}")
    public LocationDTO getLocationById(@PathVariable Integer id) {
        return locationService.getLocationById(id);
    }

    @PostMapping("/insert")
    public LocationDTO insertLocation(@RequestBody LocationDTO locationDTO) {
        return locationService.insertLocation(locationDTO);
    }

}
