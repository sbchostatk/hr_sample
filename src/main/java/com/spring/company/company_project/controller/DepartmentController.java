package com.spring.company.company_project.controller;

import com.spring.company.company_project.api.DepartmentService;
import com.spring.company.company_project.api.EmployeeService;
import com.spring.company.company_project.dto.DepartmentDTO;
import com.spring.company.company_project.dto.EmployeeDTO;
import com.spring.company.company_project.model.DepartmentEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/departments")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/getAll")
    public List<DepartmentDTO> getAllDepartments() {
        return departmentService.getAll();
    }

    @GetMapping("/{department_id}")
    public DepartmentDTO getDepartmentById(@PathVariable Integer department_id) {
        return departmentService.getDepartmentById(department_id);
    }

    @PostMapping("/insert")
    public DepartmentDTO insertDepartment(@RequestBody DepartmentDTO departmentDto) {
        return departmentService.insertDepartment(departmentDto);
    }

    @PostMapping("/update")
    public DepartmentDTO updateDepartment(@RequestBody DepartmentDTO departmentDTO) {
        return departmentService.update(departmentDTO);
    }
}
