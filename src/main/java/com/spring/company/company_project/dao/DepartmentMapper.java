package com.spring.company.company_project.dao;

import com.spring.company.company_project.dto.DepartmentDTO;
import com.spring.company.company_project.model.DepartmentEntity;
import com.spring.company.company_project.model.EmployeeEntity;
import com.spring.company.company_project.model.LocationEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface DepartmentMapper {
    @Select("select * from departments where department_id = #{id}")
    DepartmentEntity getDepartmentById(Integer id);

    @Insert("insert into departments(department_id, department_name, manager_id, location_id) values " +
            "(#{department_id}, #{department_name}, #{manager_id}, #{location_id})")
    @SelectKey(keyProperty = "department_id", before=true, resultType = Integer.class, statement = "select nextval('department_seq')")
    void insert(DepartmentEntity departmentEntity);

    List<DepartmentEntity> getAll();

    @Select("select * from departments where department_name=#{department_name}")
    List<DepartmentEntity> getAllByDepartmentName(String department_name);

    @Select("select * from departments d join employees e on d.manager_id = e.employee_id" +
            " where e.last_name=#{last_name}")
    List<DepartmentEntity> getAllByLastName(String last_name);

    @Update("update departments set department_name=#{department_name}, manager_id=#{manager_id}, location_id=#{location_id}, " +
            " where department_id=#{department_id}")
    void update(DepartmentEntity departmentEntity);
}
