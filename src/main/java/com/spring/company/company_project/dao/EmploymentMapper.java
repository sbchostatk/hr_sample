package com.spring.company.company_project.dao;

import com.spring.company.company_project.dto.EmployeeDTO;
import com.spring.company.company_project.model.EmployeeEntity;
import com.spring.company.company_project.model.JobHistoryEntity;
import com.spring.company.company_project.model.LocationEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface EmploymentMapper {
    @Select("select * from employees where employee_id = #{id}")
    EmployeeEntity getEmployeeById(Integer id);

    @Insert("insert into employees(employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) values " +
            "(#{employee_id}, #{first_name}, #{last_name}, #{email}, #{phone_number}, #{hire_date}, #{job_id}, #{salary}, #{manager_id}, #{department_id})")
    @SelectKey(keyProperty = "employee_id", before=true, resultType = Integer.class, statement = "select nextval('employee_seq')")
    void insert(EmployeeEntity employeeEntity);

    @Select("select * from employees")
    List<EmployeeEntity> getAll();

    @Update("update employees set first_name=#{first_name}, last_name=#{last_name}, email=#{email}, " +
            "phone_number=#{phone_number}, hire_date=#{hire_date}, job_id=#{job_id}, salary=#{salary}, " +
            "manager_id=#{manager_id}, department_id=#{department_id} where employee_id=#{employee_id}")
    void updateEmployee(EmployeeEntity employeeEntity);

    @Insert("insert into job_history(employee_id, start_date, end_date, job_id, department_id) values " +
            "(#{employee_id}, #{start_date}, #{end_date}, #{job_id}, #{department_id})")
    void updateHistory(JobHistoryEntity jobHistoryEntity);

    @Select("select * from employees where last_name=#{last_name}")
    List<EmployeeEntity> getAllByLastName(String last_name);

    @Select("select * from employees where first_name=#{first_name}")
    List<EmployeeEntity> getAllByFirstName(String first_name);

    @Select("select * from employees where email=#{email}")
    List<EmployeeEntity> getAllByEmail(String email);

    @Select("select * from employees e join job_history jh on e.employee_id=jh.employee_id " +
            "where jh.start_date>=#{start} and jh.end_date<=#{end}")
    List<EmployeeEntity> getAllByDate(Date start, Date end);
}
