package com.spring.company.company_project.dao;

import com.spring.company.company_project.model.LocationEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface LocationMapper {
    @Select("select * from locations where location_id = #{id}")
    Optional<LocationEntity> getLocationById(Integer id);

    @Insert("insert into locations(location_id, street_address, city) values " +
            "(#{location_id}, #{street_address}, #{city})")
    @SelectKey(keyProperty = "location_id", before=true, resultType = Integer.class, statement = "select nextval('location_seq')")
    void insert(LocationEntity locationEntity);

    @Select("select * from locations")
    List<LocationEntity> getAll();

}
