package com.spring.company.company_project.service;

import com.spring.company.company_project.api.LocationService;
import com.spring.company.company_project.dao.LocationMapper;
import com.spring.company.company_project.dto.LocationDTO;
import com.spring.company.company_project.model.LocationEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class LocationServiceImpl implements LocationService {

    private final LocationMapper locationMapper;

    private final ModelMapper modelMapper;

    public LocationServiceImpl(LocationMapper locationMapper, ModelMapper modelMapper) {
        this.locationMapper = locationMapper;
        this.modelMapper = modelMapper;
    }

    @Override
    public LocationDTO getLocationById(Integer id) {
        LocationEntity locationsEntity = locationMapper.getLocationById(id).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность локации по идентификатору=%s", id)
                )
        );
        log.debug("Получена сущность с id={}", id);
        return modelMapper.map(locationsEntity, LocationDTO.class);

    }

    @Override
    @Transactional
    public LocationDTO insertLocation(LocationDTO locationDTO) {
        LocationEntity locationEntity = modelMapper.map(locationDTO, LocationEntity.class);
        locationMapper.insert(locationEntity);
        return getLocationById(locationEntity.getLocation_id());
    }

    @Override
    public List<LocationDTO> getAll() {
        List<LocationEntity> entities = locationMapper.getAll();
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());

    }
}
