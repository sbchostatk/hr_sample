package com.spring.company.company_project.service;

import com.spring.company.company_project.api.EmployeeService;
import com.spring.company.company_project.dao.EmploymentMapper;
import com.spring.company.company_project.dto.EmployeeDTO;
import com.spring.company.company_project.dto.LocationDTO;
import com.spring.company.company_project.model.EmployeeEntity;
import com.spring.company.company_project.model.JobHistoryEntity;
import com.spring.company.company_project.model.LocationEntity;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    private final EmploymentMapper employmentMapper;
    private final ModelMapper modelMapper;

    public EmployeeServiceImpl(EmploymentMapper employmentMapper, ModelMapper modelMapper) {
        this.employmentMapper = employmentMapper;
        this.modelMapper = modelMapper;
    }

    @Override
    public EmployeeDTO getEmployeeById(Integer id) {
        EmployeeEntity employeeEntity = employmentMapper.getEmployeeById(id);
        log.debug("Получена сущность с id={}", id);
        return modelMapper.map(employeeEntity, EmployeeDTO.class);
    }

    @Override
    public EmployeeDTO insertEmployee(EmployeeDTO employeeDTO) {
        EmployeeEntity employeeEntity = modelMapper.map(employeeDTO, EmployeeEntity.class);
        employmentMapper.insert(employeeEntity);
        return getEmployeeById(employeeEntity.getEmployee_id());
    }

    @Override
    public List<EmployeeDTO> getAll() {
        List<EmployeeEntity> entities = employmentMapper.getAll();
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public EmployeeDTO update(EmployeeDTO employeeDTO) {
        EmployeeEntity newEmployee = modelMapper.map(employeeDTO, EmployeeEntity.class);
        EmployeeEntity oldEmployee = employmentMapper.getEmployeeById(newEmployee.getEmployee_id());
        employmentMapper.updateEmployee(newEmployee);
        if(!newEmployee.getJob_id().equals(oldEmployee.getJob_id()))
            employmentMapper.updateHistory(new JobHistoryEntity(newEmployee.getEmployee_id(), newEmployee.getHire_date(),
                    new Date(), newEmployee.getJob_id(), newEmployee.getDepartment_id()));
        return getEmployeeById(newEmployee.getEmployee_id());
    }

    @Override
    public List<EmployeeDTO> getAllByLastName(String last_name) {
        List<EmployeeEntity> entities = employmentMapper.getAllByLastName(last_name);
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<EmployeeDTO> getAllByFirstName(String first_name) {
        List<EmployeeEntity> entities = employmentMapper.getAllByFirstName(first_name);
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<EmployeeDTO> getAllByEmail(String email) {
        List<EmployeeEntity> entities = employmentMapper.getAllByEmail(email);
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<EmployeeDTO> getAllByDate(String start, String end) throws ParseException {
        Date start_date = new SimpleDateFormat("yyyy-MM-dd").parse(start);
        Date end_date = new SimpleDateFormat("yyyy-MM-dd").parse(end);
        List<EmployeeEntity> entities = employmentMapper.getAllByDate(start_date, end_date);
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }
}
