package com.spring.company.company_project.service;

import com.spring.company.company_project.api.DepartmentService;
import com.spring.company.company_project.dao.DepartmentMapper;
import com.spring.company.company_project.dao.EmploymentMapper;
import com.spring.company.company_project.dto.DepartmentDTO;
import com.spring.company.company_project.dto.EmployeeDTO;
import com.spring.company.company_project.model.DepartmentEntity;
import com.spring.company.company_project.model.EmployeeEntity;
import com.spring.company.company_project.model.JobHistoryEntity;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class DepartmentServiceImpl implements DepartmentService {
    private final DepartmentMapper departmentMapper;
    private final ModelMapper modelMapper;

    public DepartmentServiceImpl(DepartmentMapper departmentMapper, ModelMapper modelMapper) {
        this.departmentMapper = departmentMapper;
        this.modelMapper = modelMapper;
    }

    @Override
    public DepartmentDTO getDepartmentById(Integer id) {
        DepartmentEntity departmentEntity = departmentMapper.getDepartmentById(id);
        log.debug("Получена сущность с id={}", id);
        return modelMapper.map(departmentEntity, DepartmentDTO.class);
    }

    @Override
    public DepartmentDTO insertDepartment(DepartmentDTO departmentDTO) {
        DepartmentEntity departmentEntity = modelMapper.map(departmentDTO, DepartmentEntity.class);
        departmentMapper.insert(departmentEntity);
        return getDepartmentById(departmentEntity.getDepartment_id());
    }

    @Override
    public List<DepartmentDTO> getAll() {
        List<DepartmentEntity> entities = departmentMapper.getAll();
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public DepartmentDTO update(DepartmentDTO departmentDTO) {
        DepartmentEntity departmentEntity = modelMapper.map(departmentDTO, DepartmentEntity.class);
        departmentMapper.update(departmentEntity);
        return getDepartmentById(departmentEntity.getDepartment_id());
    }

    @Override
    public List<DepartmentDTO> getAllByDepartmentName(String department_name) {
        List<DepartmentEntity> entities = departmentMapper.getAllByDepartmentName(department_name);
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<DepartmentDTO> getAllByLastName(String last_name) {
        List<DepartmentEntity> entities = departmentMapper.getAllByLastName(last_name);
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }
}
