create table if not exists location(
                         location_id integer primary key not null unique ,
                         street_address varchar(40),
                         city varchar(30)
);

create table if not exists  departments(
                            department_id integer primary key not null unique ,
                            department_name varchar(30) ,
                            manager_id integer, --references employees(manager_id) ,
                            location_id integer references location(location_id)
);

create table if not exists jobs(
    job_id varchar(10) primary key not null ,
    job_title varchar(35) not null,
    min_salary integer,
    max_salary integer
    );

create table if not exists employees(
                          employee_id integer primary key not null unique,
                          first_name varchar(20),
                          last_name varchar(25),
                          email varchar(25) unique not null,
                          phone_number varchar(20) unique,
                          hire_date date,
                          job_id varchar(10) references jobs(job_id),
                          salary integer check ( salary>0 and salary < 1000000 ),
                          manager_id integer references employees(employee_id) not null unique,
                          department_id integer references departments(department_id)
);



create table if not exists job_history(
                            employee_id integer references employees(employee_id) not null unique,
                            start_date date not null,
                            end_date date,
                            job_id varchar(10) references  jobs(job_id),
                            department_id integer references departments(department_id),
                            primary key (employee_id,start_date)
);
alter table departments add foreign key (manager_id) references employees(manager_id);

create sequence if not exists location_seq start 1;
create sequence if not exists employee_seq start 1;
create sequence if not exists department_seq start 1;
create sequence if not exists job_seq start 1;
create sequence if not exists job_history_seq start 1;